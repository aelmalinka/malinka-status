#![warn(clippy::pedantic, clippy::nursery, missing_debug_implementations)]
use std::env;
use std::path::PathBuf;
use std::str::FromStr;

use clap::Parser;
use futures::prelude::*;
use malinka_status::error::{Error, Result};
use malinka_status::{tracing, Executor};
use snafu::prelude::*;
use tokio::fs;

#[derive(Parser, Debug)]
#[clap(author, version, about)]
struct Args {
    /// The profile to use
    #[clap(short, long, default_value = "default")]
    profile: String,
    /// Path to search for config files
    #[clap(short, long)]
    config: Option<PathBuf>,
    #[clap(
        long,
        default_value_t = num_cpus::get()
    )]
    parallelism: usize,
}

#[tokio::main]
#[snafu::report]
async fn main() -> Result {
    tracing::init()?;

    let Args {
        profile,
        config,
        parallelism,
    } = Args::parse();

    Executor::build_and_run(&resolve_config(config).await?, &profile, parallelism).await
}

async fn resolve_config(param: Option<PathBuf>) -> Result<PathBuf> {
    if let Some(p) = param {
        Ok(p)
    } else {
        Ok(PathBuf::from_str(
            &stream::iter([
                format!(
                    "{}/malinka-status/config.toml",
                    env::var("XDG_CONFIG_HOME").as_deref().unwrap_or(""),
                ),
                format!(
                    "{}/.config/malinka-status/config.toml",
                    env::var("HOME").unwrap_or_else(|_| String::new()),
                ),
                format!(
                    "{}/malinka-status.toml",
                    env::current_dir().unwrap().display(),
                ),
            ])
            .filter_map(|i| async move {
                // 2024-12-08 MLT TODO: don't crash here and document failure clearly
                if fs::try_exists(&i).await.unwrap() {
                    Some(Ok::<_, Error>(i))
                } else {
                    None
                }
            })
            .boxed()
            .try_next()
            .await?
            .whatever_context("failed to find config")?,
        )
        .unwrap())
    }
}
