//! Given a config build an executor to run the bar
use std::path::Path;
use std::str::FromStr;
use std::time::Duration;

use futures::channel::mpsc::{self, Sender};
use futures::prelude::*;
use serde::Deserialize;
use signal_hook::consts::signal::{SIGHUP, SIGINT, SIGQUIT, SIGTERM, SIGUSR1, SIGUSR2};
use signal_hook_tokio::Signals;
use snafu::prelude::*;
use tokio::{fs, task, time};
use toml::{Table, Value};

use crate::error::{Error, Result};
use crate::input::{self, Input};
use crate::output::{self, Output};

/// Serialized form
#[derive(Clone, Debug, Deserialize)]
#[serde(untagged)]
pub enum Serialized<C> {
    /// just a string
    String(String),
    /// the inner config
    Config(C),
}

/// Deserializable configs for [`Executor`]
#[derive(Clone, Debug, Deserialize)]
pub struct Config {
    output: Serialized<output::Config>,
    inputs: Vec<Serialized<input::Config>>,
    update_interval: Option<Duration>,
}

/// Wraps [`Config`] with state allowing one to "render" a set of `inputs` to an
/// `output`
#[derive(Clone, Debug, Deserialize)]
#[serde(try_from = "Config")]
pub struct Executor {
    paused: bool,

    output: output::Config,
    inputs: Vec<input::Config>,
    update_interval: Option<Duration>,
}

#[derive(Clone, Copy, Debug)]
enum Signal {
    Quit,
    Reload,
    Pause,
    Resume,
}

impl Executor {
    /// construct and run a bar given
    /// - `file` a path to a toml config file
    /// - `profile` which table to use in the config file
    /// - `parallelism` how much to parallelize the updating of inputs
    /// # Errors
    /// - as [`Self::new`]
    pub async fn build_and_run(file: &Path, profile: &str, parallelism: usize) -> Result {
        let load = || Self::new(file, profile);
        let exec = load().await?;

        exec.run(parallelism, load).await
    }

    /// construct given
    /// - `file` a path to a toml config file
    /// - `profile` which table to use in the config file
    /// # Errors
    /// - failure to open file
    /// - failure to parse as toml
    /// - failure to find given profile
    /// - failure to parse profile as [`Executor`]
    pub async fn new(file: &Path, profile: &str) -> Result<Self> {
        match <Table as FromStr>::from_str(
            &fs::read_to_string(file)
                .await
                .with_whatever_context(|_| format!("failed to open {}", file.display()))?,
        )
        .with_whatever_context(|_| format!("failed to parse {}", file.display()))?
        .remove(profile)
        .with_whatever_context(|| format!("failed to find {profile}"))?
        {
            Value::Table(t) => t,
            _ => whatever!("{profile} is not a Table"),
        }
        .try_into::<Self>()
        .with_whatever_context(|_| format!("failed to parse {profile}"))
    }

    /// given a `parallelism` and a `reload` mechanism run this Executor
    /// # Errors
    /// - as [`Self::run_once`]
    /// - failure in `reload`
    /// - failure installing signal handler
    /// - failure joining to signal handler task
    pub async fn run<F, Fut>(self, parallelism: usize, reload: F) -> Result
    where
        F: Fn() -> Fut,
        Fut: Future<Output = Result<Self>>,
    {
        run(self, parallelism, reload).await
    }

    /// given a `parallelism` run this executor one time
    /// # Errors
    /// - failure to update `inputs`
    /// - failure to update `output`
    pub async fn run_once(&mut self, parallelism: usize) -> Result {
        let Self {
            ref mut output,
            ref mut inputs,
            ..
        } = self;

        output
            .update(
                stream::iter(inputs.iter_mut().map(Input::update))
                    .map(Result::Ok)
                    .try_buffered(parallelism)
                    .try_collect::<Vec<_>>()
                    .await
                    .whatever_context("failed to update inputs")?
                    .into_iter(),
            )
            .await
            .whatever_context("failed to update output")
    }

    /// add `input` to my `inputs`
    pub fn add<I>(&mut self, input: I)
    where
        I: for<'a> Input<'a>,
    {
        self.inputs.push(input.into());
    }
}

async fn run<F, Fut>(mut exec: Executor, parallelism: usize, reload: F) -> Result
where
    F: Fn() -> Fut,
    Fut: Future<Output = Result<Executor>>,
{
    let mut interval = time::interval(
        exec.update_interval
            .unwrap_or_else(|| Duration::from_secs_f32(0.5)),
    );

    // 2024-12-08 MLT TODO: is this enough? probably
    let (tx, mut rx) = mpsc::channel(1);
    let signals = Signals::new([SIGHUP, SIGUSR1, SIGUSR2, SIGQUIT, SIGINT, SIGTERM])
        .whatever_context("failed to install signal handler")?;
    let handle = signals.handle();
    let signals_task = task::spawn(handle_signals(signals, tx));

    loop {
        futures::select_biased! {
            sig = rx.next().fuse() => match sig.unwrap() {
                Signal::Quit => break,
                Signal::Reload => {
                    exec = reload().await?;
                }
                Signal::Pause => exec.paused = true,
                Signal::Resume => exec.paused = false,
            },
            _ = interval.tick().fuse() => exec.run_once(parallelism).await?,
        }
    }

    handle.close();
    signals_task
        .await
        .whatever_context("failed to join signals task")??;

    Ok(())
}

async fn handle_signals(signals: Signals, chan: Sender<Signal>) -> Result {
    signals
        .map(|s| match s {
            SIGHUP => Signal::Reload,
            SIGUSR1 => Signal::Pause,
            SIGUSR2 => Signal::Resume,
            SIGTERM | SIGINT | SIGQUIT => Signal::Quit,
            _ => unreachable!(),
        })
        .map(Result::Ok)
        .forward(chan)
        .await
        .whatever_context("handle signals failed")
}

impl TryFrom<Config> for Executor {
    type Error = Error;

    fn try_from(
        Config {
            output,
            inputs,
            update_interval,
        }: Config,
    ) -> Result<Self, Self::Error> {
        Ok(Self {
            paused: false,
            output: output
                .try_into()
                .whatever_context("failed to convert output")?,
            inputs: inputs
                .into_iter()
                .map(TryFrom::try_from)
                .try_collect()
                .whatever_context("failed to convert inputs")?,
            update_interval,
        })
    }
}
