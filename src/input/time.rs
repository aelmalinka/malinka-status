use chrono::Local;
use serde::Deserialize;

use super::{Config, Input, Widget};
pub use crate::error::Error;

#[derive(Clone, Debug, Default, Deserialize)]
pub struct Time {
    format: Option<String>,
}

impl Input<'_> for Time {
    type Error = Error;

    async fn update(&mut self) -> Result<Widget, Error> {
        Ok(Widget {
            text: format!(
                "{}",
                Local::now().format(self.format.as_deref().unwrap_or("%F %T"))
            ),
            ..Widget::default()
        })
    }
}

impl From<Time> for Config {
    fn from(time: Time) -> Self {
        Self::Time(time)
    }
}
