use serde::Deserialize;
use snafu::prelude::*;
use tokio::fs;

use super::{Config as Cfg, Input, Widget};
pub use crate::error::Error;

const LOAD_FILE: &str = "/proc/loadavg";

#[derive(Clone, Debug, Default, Deserialize)]
pub struct Load {}

impl Input<'_> for Load {
    type Error = Error;

    async fn update(&mut self) -> Result<Widget, Error> {
        Ok(Widget {
            text: fs::read_to_string(LOAD_FILE)
                .await
                .whatever_context("failed to open load file")?
                .split_whitespace()
                .take(3)
                .map(ToOwned::to_owned)
                .collect::<Vec<_>>()
                .join(" "),
            ..Widget::default()
        })
    }
}

impl From<Load> for Cfg {
    fn from(other: Load) -> Self {
        Self::Load(other)
    }
}
