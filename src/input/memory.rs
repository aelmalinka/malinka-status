use std::collections::HashMap;

use serde::Deserialize;
use snafu::prelude::*;
use tokio::fs;

use super::{Color, Config as Cfg, Input, Level, Widget};
pub use crate::error::Error;
use crate::error::Result;

const MEMORY_FILE: &str = "/proc/meminfo";

#[derive(Clone, Debug, Default, Deserialize)]
pub struct Memory {
    warning: Option<Level>,
    critical: Option<Level>,
    prefix: Option<String>,
}

impl Input<'_> for Memory {
    type Error = Error;

    async fn update(&mut self) -> Result<Widget, Error> {
        let Self { ref prefix, .. } = self;

        let prefix = prefix.as_deref().unwrap_or("");

        let (avail, total) = Self::read().await?;
        let percent = (total - avail) * 100 / total;

        let color = self.color(percent);
        let urgent = self.urgent(percent);

        Ok(Widget {
            text: format!("{prefix}: {percent}%"),
            color,
            urgent,
            ..Widget::default()
        })
    }
}

impl Memory {
    async fn read() -> Result<(u64, u64)> {
        let values = fs::read_to_string(MEMORY_FILE)
            .await
            .whatever_context("failed to read memory file")?
            .lines()
            .filter(|line| line.starts_with("MemTotal:") || line.starts_with("MemAvailable:"))
            .filter_map(|line| {
                let (begin, end) = line.split_once(':')?;
                Some((begin.trim().to_string(), end.trim().to_string()))
            })
            .collect::<HashMap<_, _>>();

        Ok((
            Self::parse(
                values
                    .get("MemAvailable")
                    .whatever_context("missing available")?,
            )?,
            Self::parse(values.get("MemTotal").whatever_context("missing total")?)?,
        ))
    }

    fn parse(value: &str) -> Result<u64> {
        let (number, scale) = if let Some((number, unit)) = value.split_once(' ') {
            if unit == "kB" {
                (number, 1024)
            } else if unit == "mB" {
                (number, 1024 * 1024)
            } else if unit == "gB" {
                (number, 1024 * 1024 * 1024)
            } else {
                whatever!("unknown unit '{unit}'");
            }
        } else {
            (value, 1)
        };

        let number: u64 = number
            .parse()
            .with_whatever_context(|_| format!("failed to parse {number}"))?;
        Ok(number * scale)
    }

    fn color(&self, percent: u64) -> Option<Color> {
        if self.urgent(percent) {
            Some(
                self.critical
                    .as_ref()
                    .map_or(Color(255, 0, 0), |i| i.color.unwrap_or(Color(255, 0, 0))),
            )
        } else if percent
            >= self
                .warning
                .as_ref()
                .map_or(65, |i| i.amount.unwrap_or(65))
                .into()
        {
            Some(self.warning.as_ref().map_or(Color(255, 255, 0), |i| {
                i.color.unwrap_or(Color(255, 255, 0))
            }))
        } else {
            None
        }
    }

    fn urgent(&self, percent: u64) -> bool {
        percent
            >= self
                .critical
                .as_ref()
                .map_or(85, |i| i.amount.unwrap_or(85))
                .into()
    }
}

impl From<Memory> for Cfg {
    fn from(other: Memory) -> Self {
        Self::Memory(other)
    }
}
