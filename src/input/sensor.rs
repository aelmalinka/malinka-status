use std::collections::HashMap;

use serde::Deserialize;
use snafu::prelude::*;
use subprocess::Exec;
use tokio::task;

use super::{Color, Config as Cfg, Input, Widget};
pub use crate::error::Error;

// 2024-12-08 MLT TODO: colors
#[derive(Clone, Debug, Deserialize)]
pub struct Sensor {
    chip: String,
    label: String,
    prefix: Option<String>,
    suffix: Option<String>,
    round: Option<bool>,
}

impl Input<'_> for Sensor {
    type Error = Error;

    async fn update(&mut self) -> Result<Widget, Error> {
        let Self {
            ref chip,
            ref label,
            ref prefix,
            ref suffix,
            ref round,
        } = self;

        let c = chip.clone();
        let l = label.clone();

        let prefix = prefix.as_ref().unwrap_or(chip);
        let suffix = suffix.as_deref().unwrap_or("\u{00b0}C");

        let values = task::spawn_blocking(move || {
            Ok(
                { Exec::cmd("sensors").arg("-u").arg(c) | Exec::cmd("grep").arg(l) }
                    .capture()
                    .whatever_context("failed to fetch sensors")?
                    .stdout_str(),
            )
        })
        .await
        .whatever_context("failed to spawn task")??;
        let mut values = values
            .lines()
            .map(str::trim)
            // 2024-12-08 MLT TODO: hyst?
            .filter(|line| {
                line.starts_with(&format!("{label}_input"))
                    || line.starts_with(&format!("{label}_max"))
                    || line.starts_with(&format!("{label}_crit"))
            })
            .filter_map(|line| line.split_once(':'))
            .map(|(k, v)| {
                Ok((
                    k.trim(),
                    v.trim()
                        .parse::<f64>()
                        .with_whatever_context(|_| format!("failed to parse {k} ({v})"))?,
                ))
            })
            .try_collect::<HashMap<_, _>>()?;

        let input = format!("{label}_input");
        let warning = format!("{label}_max");
        let critical = format!("{label}_crit");

        let input: f64 = values.remove(&*input).whatever_context("missing input")?;
        let warning = values.remove(&*warning);
        let critical = values.remove(&*critical);

        let input = if round.unwrap_or(true) {
            input.round()
        } else {
            input
        };

        let color = if critical.is_some() && input >= critical.unwrap() {
            Some(Color(255, 0, 0))
        } else if warning.is_some() && input >= warning.unwrap() {
            Some(Color(255, 255, 0))
        } else {
            None
        };
        let urgent = critical.is_some_and(|v| input >= v);

        Ok(Widget {
            text: format!("{prefix}: {input}{suffix}"),
            color,
            urgent,
            ..Widget::default()
        })
    }
}

impl From<Sensor> for Cfg {
    fn from(other: Sensor) -> Self {
        Self::Sensor(other)
    }
}
