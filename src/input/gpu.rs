use serde::Deserialize;
use snafu::prelude::*;
use tokio::fs;

use super::{Config as Cfg, Input, Widget};
pub use crate::error::Error;

#[derive(Clone, Debug, Default, Deserialize)]
pub struct Gpu {
    card: Option<String>,
    prefix: Option<String>,
}

impl Input<'_> for Gpu {
    type Error = Error;

    async fn update(&mut self) -> Result<Widget, Error> {
        let Self {
            ref card,
            ref prefix,
        } = self;

        let card = card.as_deref().unwrap_or("card0");
        let prefix = prefix.as_deref().unwrap_or("GPU");

        let percent: u32 =
            fs::read_to_string(format!("/sys/class/drm/{card}/device/gpu_busy_percent"))
                .await
                .with_whatever_context(|_| format!("failed to read gpu percentage for '{card}'"))?
                .trim()
                .parse()
                .whatever_context("failed to parse percentage")?;

        Ok(Widget {
            text: format!("{prefix}: {percent}%"),
            ..Widget::default()
        })
    }
}

impl From<Gpu> for Cfg {
    fn from(other: Gpu) -> Self {
        Self::Gpu(other)
    }
}
