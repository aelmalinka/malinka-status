use futures::prelude::*;
use regex::Regex;
use serde::Deserialize;
use snafu::prelude::*;
use tokio::fs;
use tokio_stream::wrappers::ReadDirStream;
use tracing::instrument;

use super::{Config as Cfg, Input, Widget};
pub use crate::error::Error;
use crate::error::Result;

#[derive(Clone, Debug, Default, Deserialize)]
pub struct Battery {
    pattern: Option<String>,
    prefix: Option<String>,
}

const PATH_PREFIX: &str = "/sys/class/power_supply";

impl Input<'_> for Battery {
    type Error = Error;

    #[instrument]
    async fn update(&mut self) -> Result<Widget, Error> {
        let battery = self.pattern.as_deref().unwrap_or("BAT0");
        let prefix = self.prefix.as_deref().unwrap_or("🔋");

        tracing::debug!("fetching status and capacity");
        let status = Self::read(battery, "status").await?;
        let capacity = Self::read(battery, "capacity").await?;

        Ok(Widget {
            text: format!("{prefix}: {capacity}% ({status})"),
            ..Widget::default()
        })
    }
}

impl Battery {
    #[instrument]
    async fn read(pattern: &str, suffix: &str) -> Result<String> {
        let f = format!("{PATH_PREFIX}/{pattern}/{suffix}");
        let f2 = f.clone();

        Ok(fs::read_to_string(
            if fs::try_exists(&f)
                .await
                .with_whatever_context(|_| format!("failed to check for {f}"))?
            {
                f
            } else {
                tracing::debug!("looking for pattern");

                let r = Regex::new(&format!("^{pattern}$"))
                    .with_whatever_context(|_| format!("failed to build regex: {pattern}"))?;

                format!(
                    "{}/{}",
                    ReadDirStream::new(
                        fs::read_dir(PATH_PREFIX)
                            .await
                            .with_whatever_context(|_| format!(
                                "failed to open dir {PATH_PREFIX}"
                            ))?,
                    )
                    .try_filter(|i| future::ready(
                        r.is_match(&i.file_name().into_string().unwrap())
                    ))
                    .next()
                    .await
                    .with_whatever_context(|| format!("failed to find a match for {f}"))?
                    .with_whatever_context(|_| format!("failed to read dir entry for {f}"))?
                    .path()
                    .display(),
                    suffix
                )
            },
        )
        .await
        .with_whatever_context(|_| format!("failed to read {f2}"))?
        .trim()
        .to_string())
    }
}

impl From<Battery> for Cfg {
    fn from(other: Battery) -> Self {
        Self::Battery(other)
    }
}
