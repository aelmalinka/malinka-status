use serde::Deserialize;
use snafu::prelude::*;
use subprocess::Exec;
use tokio::task;

use super::{Config as Cfg, Input, Widget};
pub use crate::error::Error;

#[derive(Clone, Debug, Default, Deserialize)]
pub struct Nvidia {
    index: Option<u8>,
}

impl Input<'_> for Nvidia {
    type Error = Error;

    async fn update(&mut self) -> Result<Widget, Error> {
        let Self { ref index } = self;

        let index = index.unwrap_or(0);
        // let prefix = prefix.as_deref().unwrap_or("󰨇");
        // let suffix = suffix.as_deref().unwrap_or("\u{00b0}C");

        let line = task::spawn_blocking(move || {
            Ok({
                Exec::cmd("nvidia-smi")
                    .arg("-i")
                    .arg(format!("{index}"))
                    .arg("--query-gpu")
                    .arg("utilization.gpu,temperature.gpu")
                    .arg("--format=csv,noheader,nounits")
            }
            .capture()
            .whatever_context("failed to read nvidia-smi")?
            .stdout_str())
        })
        .await
        .whatever_context("failed to spawn task")??;

        let (usage, temp) = line
            .split_once(',')
            .whatever_context("invalid format returned")?;

        let usage = usage.trim();
        let temp = temp.trim();

        Ok(Widget {
            text: format!("󰨇 {usage}% {temp}°C"),
            ..Widget::default()
        })
    }
}

impl From<Nvidia> for Cfg {
    fn from(other: Nvidia) -> Self {
        Self::Nvidia(other)
    }
}
