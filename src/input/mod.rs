//! Input framework and definitions
use std::error::Error as StdError;
use std::future::Future;

use serde::{Deserialize, Serialize};
use snafu::prelude::*;
use tracing::instrument;

use crate::executor::Serialized;

mod gpu;
mod load;
mod memory;
mod time;
mod usage;

#[cfg(feature = "battery")]
mod battery;
#[cfg(feature = "disk")]
mod disk;
#[cfg(feature = "network")]
mod network;
#[cfg(feature = "nvidia")]
mod nvidia;
#[cfg(feature = "sensor")]
mod sensor;

/// A color given as r, g, b
// 2024-12-08 MLT TODO: alpha channel?
#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
#[serde(into = "String")]
pub struct Color(pub u8, pub u8, pub u8);

/// A Color and an Amount
#[derive(Clone, Debug, Default, Deserialize)]
pub struct Level {
    color: Option<Color>,
    amount: Option<u32>,
}

/// A representation of the state of an input
/// this should be enough to render the given input
#[derive(Clone, Debug, Default)]
pub struct Widget {
    /// the text to display
    pub text: String,
    /// an optional tooltip to display on cursor over
    pub tooltip: Option<String>,
    /// the color of the background
    // 2024-12-08 MLT TODO: foreground
    pub color: Option<Color>,
    /// if this `Widget` is flagged as urgent
    pub urgent: bool,
}

/// How to supply a new [`Widget`] component to the bar
pub trait Input<'a>: Into<Config> + Deserialize<'a> + Send {
    /// the Error type returned by [`Self::update`]
    type Error: StdError + Send;

    /// update this component by updating it's state
    fn update(&mut self) -> impl Future<Output = Result<Widget, Self::Error>> + Send;
}

// 2024-12-08 MLT TODO: volume
/// A wrapper around built-in [`Input`]s used for config deserialization
#[derive(Clone, Debug, Deserialize)]
#[serde(tag = "name")]
pub enum Config {
    /// Gpu usage percentage
    Gpu(gpu::Gpu),
    /// Load Averages
    Load(load::Load),
    /// Memory used percent
    Memory(memory::Memory),
    /// System time
    Time(time::Time),
    /// CPU usage
    Usage(usage::Usage),

    /// Battery status/percentage via acpi
    #[cfg(feature = "battery")]
    Battery(battery::Battery),
    /// Disk used percent
    #[cfg(feature = "disk")]
    Disk(disk::Disk),
    /// Network status/address
    #[cfg(feature = "network")]
    Network(network::Network),
    /// Nvidia specific nonsense
    #[cfg(feature = "nvidia")]
    Nvidia(nvidia::Nvidia),
    /// Sensor output (usually for temperature)
    #[cfg(feature = "sensor")]
    Sensor(sensor::Sensor),
}

/// A wrapper around built-in [`Input`]s errors
#[derive(Debug, Snafu)]
pub enum Error {
    /// an error in `Gpu`
    Gpu {
        #[doc(hidden)]
        source: gpu::Error,
    },
    /// an error in `Load`
    Load {
        #[doc(hidden)]
        source: load::Error,
    },
    /// an error in `Memory`
    Memory {
        #[doc(hidden)]
        source: memory::Error,
    },
    /// an error in `Time`
    Time {
        #[doc(hidden)]
        source: time::Error,
    },
    /// an error in `Usage`
    Usage {
        #[doc(hidden)]
        source: usage::Error,
    },

    /// an error in `Battery`
    #[cfg(feature = "battery")]
    Battery {
        #[doc(hidden)]
        source: battery::Error,
    },
    /// an error in `Time`
    #[cfg(feature = "disk")]
    Disk {
        #[doc(hidden)]
        source: disk::Error,
    },
    /// an error in `Network`
    #[cfg(feature = "network")]
    Network {
        #[doc(hidden)]
        source: network::Error,
    },
    /// an error in `Nvidia`
    #[cfg(feature = "nvidia")]
    Nvidia {
        #[doc(hidden)]
        source: nvidia::Error,
    },
    /// an error in `Sensor`
    #[cfg(feature = "sensor")]
    Sensor {
        #[doc(hidden)]
        source: sensor::Error,
    },
    // 2024-12-09 MLT TODO:
    /// Uncategorized error
    #[snafu(whatever, display("{message}"))]
    Whatever {
        #[doc(hidden)]
        message: String,
        #[doc(hidden)]
        #[snafu(source(from(Box<dyn std::error::Error + Send + Sync>, Some)))]
        source: Option<Box<dyn std::error::Error + Send + Sync>>,
    },
}

impl Input<'_> for Config {
    type Error = Error;

    #[instrument]
    async fn update(&mut self) -> Result<Widget, Error> {
        match self {
            Self::Gpu(ref mut inner) => inner.update().context(GpuSnafu).await,
            Self::Load(ref mut inner) => inner.update().context(LoadSnafu).await,
            Self::Memory(ref mut inner) => inner.update().context(MemorySnafu).await,
            Self::Time(ref mut inner) => inner.update().context(TimeSnafu).await,
            Self::Usage(ref mut inner) => inner.update().context(UsageSnafu).await,
            #[cfg(feature = "battery")]
            Self::Battery(ref mut inner) => inner.update().context(BatterySnafu).await,
            #[cfg(feature = "disk")]
            Self::Disk(ref mut inner) => inner.update().context(DiskSnafu).await,
            #[cfg(feature = "network")]
            Self::Network(ref mut inner) => inner.update().context(NetworkSnafu).await,
            #[cfg(feature = "nvidia")]
            Self::Nvidia(ref mut inner) => inner.update().context(NvidiaSnafu).await,
            #[cfg(feature = "sensor")]
            Self::Sensor(ref mut inner) => inner.update().context(SensorSnafu).await,
        }
    }
}

impl From<Color> for String {
    #[instrument]
    fn from(color: Color) -> Self {
        let Color(r, g, b) = color;
        format!("#{r:02x}{g:02x}{b:02x}")
    }
}

impl TryFrom<Serialized<Self>> for Config {
    type Error = Error;

    #[instrument]
    fn try_from(other: Serialized<Self>) -> Result<Self, Self::Error> {
        Ok(match other {
            Serialized::String(inner) if inner == "Gpu" => Self::Gpu(gpu::Gpu::default()),
            Serialized::String(inner) if inner == "Load" => Self::Load(load::Load::default()),
            Serialized::String(inner) if inner == "Memory" => {
                Self::Memory(memory::Memory::default())
            }
            Serialized::String(inner) if inner == "Time" => Self::Time(time::Time::default()),
            Serialized::String(inner) if inner == "Usage" => Self::Usage(usage::Usage::default()),
            #[cfg(feature = "battery")]
            Serialized::String(inner) if inner == "Battery" => {
                Self::Battery(battery::Battery::default())
            }
            #[cfg(feature = "disk")]
            Serialized::String(inner) if inner == "Disk" => Self::Disk(disk::Disk::default()),
            #[cfg(feature = "network")]
            Serialized::String(inner) if inner == "Network" => {
                Self::Network(network::Network::default())
            }
            #[cfg(feature = "nvidia")]
            Serialized::String(inner) if inner == "Nvidia" => {
                Self::Nvidia(nvidia::Nvidia::default())
            }
            Serialized::Config(inner) => inner,
            _ => whatever!("no match"),
        })
    }
}
