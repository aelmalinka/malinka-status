use serde::Deserialize;
use snafu::prelude::*;
use tokio::fs;

use super::{Color, Config as Cfg, Input, Level, Widget};
pub use crate::error::{Error, Result};

const STAT_FILE: &str = "/proc/stat";

#[derive(Clone, Debug, Default, Deserialize)]
pub struct Config {
    warning: Option<Level>,
    critical: Option<Level>,
}

#[derive(Clone, Debug, Default, Deserialize)]
#[serde(from = "Config")]
pub struct Usage {
    warning: Option<Level>,
    critical: Option<Level>,

    prev_total: u32,
    prev_idle: u32,
}

impl Usage {
    fn color(&self, percent: u32) -> Option<Color> {
        if self.urgent(percent) {
            Some(
                self.critical
                    .as_ref()
                    .map_or(Color(255, 0, 0), |i| i.color.unwrap_or(Color(255, 0, 0))),
            )
        } else if percent >= self.warning.as_ref().map_or(65, |i| i.amount.unwrap_or(65)) {
            Some(self.warning.as_ref().map_or(Color(255, 255, 0), |i| {
                i.color.unwrap_or(Color(255, 255, 0))
            }))
        } else {
            None
        }
    }

    fn urgent(&self, percent: u32) -> bool {
        percent
            >= self
                .critical
                .as_ref()
                .map_or(85, |i| i.amount.unwrap_or(85))
    }

    async fn read() -> Result<Vec<u32>> {
        fs::read_to_string(STAT_FILE)
            .await
            .whatever_context("failed to read stat file")?
            .lines()
            .filter(|line| line.starts_with("cpu "))
            .flat_map(|line| {
                line.split(char::is_whitespace)
                    .map(str::to_owned)
                    .collect::<Vec<_>>()
                    .into_iter()
                    .skip(2)
            })
            .map(|i| {
                i.parse::<u32>()
                    .with_whatever_context(|_| format!("incorrect entry in cpu line: {i}"))
            })
            .try_collect()
    }
}

impl Input<'_> for Usage {
    type Error = Error;

    async fn update(&mut self) -> Result<Widget, Error> {
        let Self {
            ref mut prev_total,
            ref mut prev_idle,
            ..
        } = self;

        let mut percent = 0;
        let line = Self::read().await?;

        match line.as_slice() {
            [user, nice, system, idle, io_wait, irq, soft_irq, steal, _, _] => {
                let total = user + nice + system + idle + io_wait + irq + soft_irq + steal;
                let idle = idle + io_wait;

                let diff_total = total - *prev_total;
                let diff_idle = idle - *prev_idle;

                *prev_total = total;
                *prev_idle = idle;

                if diff_total != 0 {
                    percent = (diff_total - diff_idle) * 100 / diff_total;
                }
            }
            _ => whatever!("bad cpu line"),
        }

        Ok(Widget {
            text: format!("{percent}%"),
            color: self.color(percent),
            urgent: self.urgent(percent),
            ..Widget::default()
        })
    }
}

impl From<Config> for Usage {
    fn from(Config { warning, critical }: Config) -> Self {
        Self {
            warning,
            critical,
            ..Self::default()
        }
    }
}

impl From<Usage> for Cfg {
    fn from(usage: Usage) -> Self {
        Self::Usage(usage)
    }
}
