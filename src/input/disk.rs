use serde::Deserialize;
use snafu::prelude::*;
use subprocess::Exec;
use tokio::task;

use super::{Config as Cfg, Input, Widget};
pub use crate::error::Error;

#[derive(Clone, Debug, Default, Deserialize)]
pub struct Disk {
    mount: Option<String>,
}

impl Input<'_> for Disk {
    type Error = Error;

    async fn update(&mut self) -> Result<Widget, Error> {
        let Self { ref mount } = self;

        let mount = mount.as_deref().unwrap_or("/").to_owned();
        let mnt_name = mount.clone();

        Ok(Widget {
            text: format!(
                "{}: {}",
                mnt_name,
                task::spawn_blocking(move || {
                    Ok({
                        Exec::cmd("df").arg("--output=pcent").arg(mount)
                            | Exec::cmd("tail").arg("-n1")
                    }
                    .capture()
                    .whatever_context("failed to read disk")?
                    .stdout_str())
                })
                .await
                .whatever_context("failed to spawn task")??
                .trim()
            ),
            ..Widget::default()
        })
    }
}

impl From<Disk> for Cfg {
    fn from(other: Disk) -> Self {
        Self::Disk(other)
    }
}
