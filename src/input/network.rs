use serde::Deserialize;
use snafu::prelude::*;
use subprocess::Exec;
use tokio::task;

use super::{Color, Config as Cfg, Input, Widget};
pub use crate::error::Error;

#[derive(Clone, Debug, Default, Deserialize)]
pub struct Network {
    adapter: Option<String>,
}

impl Input<'_> for Network {
    type Error = Error;

    async fn update(&mut self) -> Result<Widget, Error> {
        let Self { ref adapter } = self;

        let adapter = match adapter.clone() {
            Some(adapter) => adapter,
            None => task::spawn_blocking(|| {
                Ok({
                    Exec::cmd("ip").arg("link")
                        | Exec::cmd("grep").arg("-v").arg("master")
                        | Exec::cmd("awk").arg("/ UP /{print $2}")
                        | Exec::cmd("sed").arg("s,:,,g")
                }
                .capture()
                .whatever_context("failed to get adapter")?
                .stdout_str())
            })
            .await
            .whatever_context("failed to spawn adapter getting task")??
            .trim()
            .to_owned(),
        };

        // 2024-12-08 MLT TODO: default prefix to icons detected from network type
        let prefix = adapter.clone();

        let ip = task::spawn_blocking(move || {
            Ok({
                Exec::cmd("ip")
                    .arg("addr")
                    .arg("show")
                    .arg("dev")
                    .arg(adapter)
                    | Exec::cmd("awk").arg("/inet /{print $2}")
            }
            .capture()
            .whatever_context("failed to get ip")?
            .stdout_str())
        })
        .await
        .whatever_context("failed to spawn ip task")??
        .trim()
        .to_owned();

        let urgent = ip.is_empty();
        let ipstr = if urgent { "Down" } else { &ip };
        let color = if urgent { Some(Color(255, 0, 0)) } else { None };

        Ok(Widget {
            text: format!("{prefix}: {ipstr}"),
            color,
            urgent,
            ..Widget::default()
        })
    }
}

impl From<Network> for Cfg {
    fn from(other: Network) -> Self {
        Self::Network(other)
    }
}
