//! Tracing supports
use snafu::prelude::*;
use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use tracing_subscriber::fmt::Layer;
use tracing_subscriber::prelude::*;

use crate::error::Result;

/// Initialize tracing layers
/// # Errors
/// - [`tracing_subscriber::registry()`]
pub fn init() -> Result {
    tracing_subscriber::registry()
        .with(
            Layer::default().with_filter(
                EnvFilter::builder()
                    .with_default_directive(LevelFilter::INFO.into())
                    .from_env_lossy(),
            ),
        )
        .try_init()
        .whatever_context("failed to install tracing")
}
