//! Error handling
use snafu::prelude::*;

/// Global error type
#[derive(Debug, Snafu)]
pub enum Error {
    /// Uncategorized error
    #[snafu(whatever, display("{message}"))]
    Whatever {
        #[doc(hidden)]
        message: String,
        #[doc(hidden)]
        #[snafu(source(from(Box<dyn std::error::Error + Send + Sync>, Some)))]
        source: Option<Box<dyn std::error::Error + Send + Sync>>,
    },
}

/// Result type used by this program
pub type Result<T = (), E = Error> = std::result::Result<T, E>;
