//! An implementation of a Layer Shell bar
use serde::Deserialize;
use snafu::prelude::*;

use super::{Output, Widget};

// 2024-12-08 MLT TODO: which screens to render on
/// Where to render the bar
#[derive(Clone, Copy, Debug, Default, Deserialize)]
pub enum Position {
    /// Bottom of the screen
    #[default]
    Bottom,
    /// Top of the screen
    Top,
    /// Left of the screen
    Left,
    /// Right of the screen
    Right,
}

/// Deserializable configs for [`self`]
#[derive(Clone, Debug, Deserialize)]
#[serde(default)]
pub struct Config {
    width: Option<u32>,
    height: Option<u32>,
    dock: Position,
}

// 2024-12-08 MLT TODO: separate into Config
/// Status/Config for this bar
#[derive(Clone, Debug, Deserialize)]
#[serde(from = "Config")]
pub struct Bar {
    width: Option<u32>,
    height: Option<u32>,
    dock: Position,
}

/// An error when running [`Bar::update`]
#[derive(Debug, Snafu)]
pub enum Error {
    /// Something else (todo)
    Whatever {},
}

impl Output for Bar {
    type Error = Error;

    async fn update(&mut self, _: impl Iterator<Item = Widget> + Send) -> Result<(), Error> {
        Ok::<_, Error>(())
    }
}

impl From<Config> for Bar {
    fn from(
        Config {
            width,
            height,
            dock,
        }: Config,
    ) -> Self {
        Self {
            width,
            height,
            dock,
        }
    }
}

impl Default for Config {
    fn default() -> Self {
        Self {
            width: 20.into(),
            height: None,
            dock: Position::default(),
        }
    }
}

impl Default for Bar {
    fn default() -> Self {
        Self {
            width: 20.into(),
            height: None,
            dock: Position::default(),
        }
    }
}
