//! Output framework and definitions
use std::error::Error as StdError;
use std::fmt;
use std::future::Future;

use serde::Deserialize;
use snafu::prelude::*;
use tracing::instrument;

use crate::executor::Serialized;
#[cfg(doc)]
use crate::input::Input;
use crate::input::Widget;

#[cfg(any(feature = "i3", doc))]
pub mod i3;
#[cfg(any(feature = "wayland", doc))]
pub mod wayland;

/// How to take [`Input`]s and render them out
pub trait Output: fmt::Debug + Send {
    /// the Error type returned by [`Self::update`]
    type Error: StdError + Send;

    /// update this output (i.e. render it; send one full message; etc)
    fn update(
        &mut self,
        data: impl Iterator<Item = Widget> + Send,
    ) -> impl Future<Output = Result<(), Self::Error>> + Send;
}

/// A wrapper around built-in [`Output`]s
#[derive(Clone, Debug, Deserialize)]
#[serde(tag = "name")]
pub enum Config {
    /// see [`i3`]
    #[cfg(any(feature = "i3", doc))]
    I3(i3::Bar),
    /// see [`wayland`]
    #[cfg(any(feature = "wayland", doc))]
    Wayland(wayland::Bar),
}

/// A wrapper around built-in [`Output`]s
#[derive(Debug, Snafu)]
pub enum Error {
    #[cfg(any(feature = "i3", doc))]
    /// see [`i3::Error`]
    I3 {
        #[doc(hidden)]
        source: i3::Error,
    },
    #[cfg(any(feature = "wayland", doc))]
    /// see [`wayland::Error`]
    Wayland {
        #[doc(hidden)]
        source: wayland::Error,
    },
    // 2024-12-09 MLT TODO:
    /// Uncategorized error
    #[snafu(whatever, display("{message}"))]
    Whatever {
        #[doc(hidden)]
        message: String,
        #[doc(hidden)]
        #[snafu(source(from(Box<dyn std::error::Error + Send + Sync>, Some)))]
        source: Option<Box<dyn std::error::Error + Send + Sync>>,
    },
}

impl Output for Config {
    type Error = Error;

    #[instrument(skip(data))]
    async fn update(&mut self, data: impl Iterator<Item = Widget> + Send) -> Result<(), Error> {
        match self {
            #[cfg(feature = "i3")]
            Self::I3(ref mut inner) => inner.update(data).context(I3Snafu).await,
            #[cfg(feature = "wayland")]
            Self::Wayland(ref mut inner) => inner.update(data).context(WaylandSnafu).await,
            #[cfg(doc)]
            _ => panic!("this is only for docs"),
        }
    }
}

impl TryFrom<Serialized<Self>> for Config {
    type Error = Error;

    #[instrument]
    fn try_from(other: Serialized<Self>) -> Result<Self, Self::Error> {
        Ok(match other {
            #[cfg(feature = "i3")]
            Serialized::String(inner) if inner == "I3" => Self::I3(i3::Bar::default()),
            #[cfg(feature = "wayland")]
            Serialized::String(inner) if inner == "Wayland" => {
                Self::Wayland(wayland::Bar::default())
            }
            Serialized::Config(inner) => inner,
            _ => whatever!("no match"),
        })
    }
}
