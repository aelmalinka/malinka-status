//! Bar rendering library
#![warn(
    clippy::pedantic,
    clippy::nursery,
    missing_docs,
    missing_debug_implementations
)]
#![feature(iterator_try_collect)]

pub mod error;
pub mod executor;
pub mod input;
pub mod output;
pub mod tracing;

pub use executor::Executor;
